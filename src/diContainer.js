import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';
import AddPartnerCommercialSaleRegDraftFeature from './addPartnerCommercialSaleRegDraftFeature';
import GetPartnerCommercialSaleRegDraftWithIdFeature from './getPartnerCommercialSaleRegDraftWithIdFeature';
import GetPartnerCommercialSaleRegDraftWithAccountIdFeature from './getPartnerCommercialSaleRegDraftWithAccountIdFeature';
import SubmitPartnerCommercialSaleRegDraftFeature from './submitPartnerCommercialSaleRegDraftFeature';
import UpdatePartnerCommercialSaleRegDraftFeature from './updatePartnerCommercialSaleRegDraftFeature';
import DeletePartnerSaleRegDraftWithIdFeature from './deletePartnerSaleRegDraftWithIdFeature';
import UpdateSaleInvoiceUrlOfPartnerSaleRegDraftFeature from './updateSaleInvoiceUrlOfPartnerSaleRegDraftFeature';
import RemoveSaleLineItemFromPartnerSaleRegDraftFeature from './removeSaleLineItemFromPartnerSaleRegDraftFeature';


/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {PartnerSaleRegistrationDraftServiceSdkConfig} config
     */
    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(PartnerSaleRegistrationDraftServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(AddPartnerCommercialSaleRegDraftFeature);
        this._container.autoRegister(GetPartnerCommercialSaleRegDraftWithIdFeature);
        this._container.autoRegister(GetPartnerCommercialSaleRegDraftWithAccountIdFeature);
        this._container.autoRegister(SubmitPartnerCommercialSaleRegDraftFeature);
        this._container.autoRegister(UpdatePartnerCommercialSaleRegDraftFeature);
        this._container.autoRegister(DeletePartnerSaleRegDraftWithIdFeature);
        this._container.autoRegister(UpdateSaleInvoiceUrlOfPartnerSaleRegDraftFeature);
        this._container.autoRegister(RemoveSaleLineItemFromPartnerSaleRegDraftFeature);

    }

}

import PartnerSaleLineItemComponent from './partnerSaleLineItemComponent';
/**
 * @class {PartnerSaleRegDraftSaleCompositeLineItem}
 */
export default class PartnerSaleRegDraftSaleCompositeLineItem{

    _id:number;

    _components:PartnerSaleLineItemComponent[];

    _price:number;

    /**
     * @param id
     * @param components
     * @param price
     */
    constructor(id:number,
                components:PartnerSaleLineItemComponent[],
                price:number
    ){

        this._id = id;

        if(!components){
            throw new TypeError('components required');
        }
        this._components = components;

        this._price = price;
    }

    /**
     *
     * @returns {number}
     */
    get id():number{
        return this._id;
    }

    get components():PartnerSaleLineItemComponent[]{
        return this._components;
    }

    get price():number{
        return this._price;
    }


    toJSON(){
        return {
            id:this._id,
            components:this._components,
            price:this._price
        }
    }
}

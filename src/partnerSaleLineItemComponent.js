/**
 * @class {PartnerSaleLineItemComponent}
 */
export default class PartnerSaleLineItemComponent{

    _assetId:string;

    _price:number;

    constructor(assetId:string,
                price:number,
    ){

        if(!assetId){
            throw new TypeError('assetId required');
        }
        this._assetId = assetId;

        this._price = price;

    }

    /**
     * @returns {string}
     */
    get assetId():string{
        return this._assetId;
    }

    get price():number{
        return this._price;
    }

    toJSON(){
        return {
            assetId:this._assetId,
            price:this._price
        }
    }
}

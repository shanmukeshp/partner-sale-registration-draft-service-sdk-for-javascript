import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import AddPartnerCommercialSaleRegDraftReq from './addPartnerCommercialSaleRegDraftReq';
import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';

@inject(PartnerSaleRegistrationDraftServiceSdkConfig, HttpClient)
class DeletePartnerSaleRegDraftWithIdFeature {

    _config:PartnerSaleRegistrationDraftServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param partnerSaleRegDraftId
     * @param accessToken
     * @returns {Promise.<T>}
     */
    execute(partnerSaleRegDraftId:number,
            accessToken:string):Promise<number> {

        return this._httpClient
            .createRequest(`partner-sale-registration/deletepartnersaleregdraft/${partnerSaleRegDraftId}`)
            .asDelete()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => response.content);
    }
}

export default DeletePartnerSaleRegDraftWithIdFeature;

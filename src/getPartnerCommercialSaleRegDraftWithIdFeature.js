import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import PartnerCommercialSaleRegSynopsisView from './partnerCommercialSaleRegSynopsisView';
import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';

@inject(PartnerSaleRegistrationDraftServiceSdkConfig, HttpClient)
class GetPartnerCommercialSaleRegDraftWithIdFeature {

    _config:PartnerSaleRegistrationDraftServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    execute(partnerSaleRegDraftId:number,
            accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView> {

        return this._httpClient
            .createRequest(`partner-sale-registration/draftId/${partnerSaleRegDraftId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => response.content);
    }
}

export default GetPartnerCommercialSaleRegDraftWithIdFeature;

import PartnerSaleRegistrationDraftServiceSdkConfig from './partnerSaleRegistrationDraftServiceSdkConfig';
import DiContainer from './diContainer';
import AddPartnerCommercialSaleRegDraftReq from './addPartnerCommercialSaleRegDraftReq';
import PartnerCommercialSaleRegDraftView from './partnerCommercialSaleRegDraftView';
import AddPartnerCommercialSaleRegDraftFeature from './addPartnerCommercialSaleRegDraftFeature';
import PartnerCommercialSaleRegSynopsisView from './partnerCommercialSaleRegSynopsisView';
import GetPartnerCommercialSaleRegDraftWithIdFeature from './getPartnerCommercialSaleRegDraftWithIdFeature';
import GetPartnerCommercialSaleRegDraftWithAccountIdFeature from './getPartnerCommercialSaleRegDraftWithAccountIdFeature';
import SubmitPartnerCommercialSaleRegDraftFeature from './submitPartnerCommercialSaleRegDraftFeature';
import UpdatePartnerCommercialSaleRegDraftReq from './updatePartnerCommercialSaleRegDraftReq';
import UpdatePartnerCommercialSaleRegDraftFeature from './updatePartnerCommercialSaleRegDraftFeature';
import DeletePartnerSaleRegDraftWithIdFeature from './deletePartnerSaleRegDraftWithIdFeature';
import UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq from './updateSaleInvoiceUrlOfPartnerSaleRegDraftReq';
import UpdateSaleInvoiceUrlOfPartnerSaleRegDraftFeature from './updateSaleInvoiceUrlOfPartnerSaleRegDraftFeature';
import RemoveSaleLineItemFromPartnerSaleRegDraftFeature from './removeSaleLineItemFromPartnerSaleRegDraftFeature';

/**
 * @class {PartnerSaleRegistrationServiceSdk}
 */
export default class PartnerSaleRegistrationServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {PartnerSaleRegistrationDraftServiceSdkConfig} config
     */
    constructor(config:PartnerSaleRegistrationDraftServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     *
     * @param request
     * @param accessToken
     * @returns {Promise.<number>}
     */
    addPartnerCommercialSaleRegDraft(
        request:AddPartnerCommercialSaleRegDraftReq,
        accessToken:string):Promise<number>{

        return this
            ._diContainer
            .get(AddPartnerCommercialSaleRegDraftFeature)
            .execute(
                request,
                accessToken
            );
    }

    /**
     *
     * @param partnerSaleRegDraftId
     * @param accessToken
     * @returns {Promise.<PartnerCommercialSaleRegSynopsisView>}
     */
    getPartnerSaleRegistrationDraftWithDraftId(
        partnerSaleRegDraftId:number,
        accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView> {

        return this
            ._diContainer
            .get(GetPartnerCommercialSaleRegDraftWithIdFeature)
            .execute(partnerSaleRegDraftId,accessToken);
    }

    /**
     *
     * @param partnerAccountId
     * @param accessToken
     * @returns {Promise.<PartnerCommercialSaleRegSynopsisView[]>}
     */
    getPartnerCommercialSaleRegDraftWithAccountId(
        partnerAccountId:string,
        accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView[]>{

        return this
            ._diContainer
            .get(GetPartnerCommercialSaleRegDraftWithAccountIdFeature)
            .execute(
                partnerAccountId,
                accessToken
            );
    }

    /**
     *
     * @param partnerSaleRegDraftId
     * @param accessToken
     * @returns {Promise.<number>|Promise.<PartnerCommercialSaleRegSynopsisView>}
     */
    submitPartnerCommercialSaleRegDraft(
        partnerSaleRegDraftId:number,
        accessToken:string):Promise<PartnerCommercialSaleRegDraftView>{

        return this
            ._diContainer
            .get(SubmitPartnerCommercialSaleRegDraftFeature)
            .execute(
                partnerSaleRegDraftId,
                accessToken
            );
    }

    /**
     *
     * @param request
     * @param partnerSaleRegDraftId
     * @param accessToken
     * @returns {PartnerCommercialSaleRegSynopsisView}
     */
    updatePartnerCommercialSaleRegDraft(request:UpdatePartnerCommercialSaleRegDraftReq,
                                        partnerSaleRegDraftId:number,
                                        accessToken:string):Promise<PartnerCommercialSaleRegSynopsisView>{
        return this
            ._diContainer
            .get(UpdatePartnerCommercialSaleRegDraftFeature)
            .execute(
                request,
                partnerSaleRegDraftId,
                accessToken
            );
    }

    /**
     *
     * @param partnerSaleRegDraftId
     * @param accessToken
     * @returns {Promise.<number>|Promise.<PartnerCommercialSaleRegSynopsisView[]>|Promise.<PartnerCommercialSaleRegSynopsisView>}
     */
    deletePartnerSaleRegDraftWithId(partnerSaleRegDraftId:number,accessToken:string){

        return this
            ._diContainer
            .get(DeletePartnerSaleRegDraftWithIdFeature)
            .execute(
                partnerSaleRegDraftId,
                accessToken
            );

    }

    /**
     * @param request
     * @param accessToken
     * @returns {string}
     */
    updateSaleInvoiceUrlOfPartnerSaleRegDraft(request:UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq,
                                              accessToken:string){

        return this
            ._diContainer
            .get(UpdateSaleInvoiceUrlOfPartnerSaleRegDraftFeature)
            .execute(
                request,
                accessToken
            );

    }

    /**
     * @param partnerSaleRegDraftSaleLineItemId
     * @param accessToken
     * @returns {string}
     */
    removeSaleLineItemFromPartnerSaleRegDraft(partnerSaleRegDraftSaleLineItemId:number,
                                              accessToken:string):Promise<string>{

        return this
            ._diContainer
            .get(RemoveSaleLineItemFromPartnerSaleRegDraftFeature)
            .execute(
                partnerSaleRegDraftSaleLineItemId,
                accessToken
            );
    }

}

import PartnerSaleRegDraftSaleSimpleLineItem from './partnerSaleRegDraftSaleSimpleLineItem';
import PartnerSaleRegDraftSaleCompositeLineItem from './partnerSaleRegDraftSaleCompositeLineItem';

/**
 * @class {PartnerCommercialSaleRegDraftView}
 */
export default class PartnerCommercialSaleRegDraftView{

    _id:number;

    _facilityContactId:string;

    _facilityId:string;

    _partnerAccountId:string;

    _sellDate:string;

    _installDate:string;

    _draftSubmittedDate:string;

    _customerSourceId:number;

    _managementCompanyId:string;

    _buyingGroupId:string;

    _partnerRepUserId:string;

    _invoiceUrl:string;

    _isSubmitted:boolean;

    _simpleLineItems:PartnerSaleRegDraftSaleSimpleLineItem[] ;

    _compositeLineItems:PartnerSaleRegDraftSaleCompositeLineItem[];

    /**
     *
     * @param id
     * @param facilityContactId
     * @param facilityId
     * @param partnerAccountId
     * @param sellDate
     * @param installDate
     * @param draftSubmittedDate
     * @param invoiceNumber
     * @param customerSourceId
     * @param managementCompanyId
     * @param buyingGroupId
     * @param partnerRepUserId
     * @param invoiceUrl
     * @param isSubmitted
     * @param simpleLineItems
     * @param compositeLineItems
     */
    constructor(id,
                facilityContactId,
                facilityId,
                partnerAccountId,
                sellDate,
                installDate,
                draftSubmittedDate,
                invoiceNumber,
                customerSourceId,
                managementCompanyId,
                buyingGroupId,
                partnerRepUserId,
                invoiceUrl,
                isSubmitted,
                simpleLineItems,
                compositeLineItems
    ){

        if(!id){
            throw new TypeError('id is required');
        }
        this._id = id;

        if(!facilityContactId){
            throw new TypeError('facilityContactId is required');
        }
        this._facilityContactId = facilityContactId;

        if(!facilityId){
            throw new TypeError('facilityId is required');
        }
        this._facilityId = facilityId;

        if(!partnerAccountId){
            throw new TypeError('partnerAccountId is required');
        }
        this._partnerAccountId = partnerAccountId;

        if(!sellDate){
            throw new TypeError('sellDate is required');
        }
        this._sellDate = sellDate;

        if(!installDate){
            throw new TypeError('installDate is required');
        }
        this._installDate = installDate;

        if(!draftSubmittedDate){
            throw new TypeError('draftSubmittedDate is required');
        }
        this._draftSubmittedDate = draftSubmittedDate;

        if(!invoiceNumber){
            throw new TypeError('invoiceNumber is required');
        }
        this._invoiceNumber = invoiceNumber;

        this._customerSourceId = customerSourceId;

        this._managementCompanyId = managementCompanyId;

        this._buyingGroupId = buyingGroupId;

        this._partnerRepUserId = partnerRepUserId;

        this._invoiceUrl = invoiceUrl;

        this._isSubmitted = isSubmitted;

        this._simpleLineItems = simpleLineItems;

        this._compositeLineItems = compositeLineItems;

    }

    /**
     * getter methods
     */
    get id():number{
        return this._id;
    }

    get facilityContactId():string{
        return this._facilityContactId;
    }

    get facilityId():string{
        return this._facilityId;
    }

    get partnerAccountId():string{
        return this._partnerAccountId;
    }

    get sellDate():string{
        return this._sellDate;
    }

    get installDate():string{
        return this._installDate;
    }

    get draftSubmittedDate():string{
        return this._draftSubmittedDate;
    }

    get invoiceNumber():string{
        return this._invoiceNumber;
    }

    get customerSourceId():number{
        return this._customerSourceId;
    }

    get managementCompanyId():string{
        return this._managementCompanyId;
    }

    get buyingGroupId():string{
        return this._buyingGroupId;
    }

    get partnerRepUserId():string{
        return this._partnerRepUserId;
    }

    get invoiceUrl():string{
        return this._invoiceUrl;
    }

    get isSubmitted():boolean{
        return this._isSubmitted;
    }

    get simpleLineItems():PartnerSaleRegDraftSaleSimpleLineItem[]{
        return this._simpleLineItems;
    }

    get compositeLineItems():PartnerSaleRegDraftSaleCompositeLineItem[]{
        return this._compositeLineItems;
    }

    toJSON() {
        return {
            id:this._id,
            facilityContactId:this._facilityContactId,
            facilityId:this._facilityId,
            partnerAccountId:this._partnerAccountId,
            sellDate:this._sellDate,
            installDate:this._installDate,
            draftSubmittedDate:this._draftSubmittedDate,
            invoiceNumber:this._invoiceNumber,
            customerSourceId:this._customerSourceId,
            managementCompanyId:this._managementCompanyId,
            buyingGroupId:this._buyingGroupId,
            partnerRepUserId:this._partnerRepUserId,
            invoiceUrl:this._invoiceUrl,
            isSubmitted:this._isSubmitted,
            simpleLineItems:this._simpleLineItems,
            compositeLineItems:this._compositeLineItems
        }
    };
}


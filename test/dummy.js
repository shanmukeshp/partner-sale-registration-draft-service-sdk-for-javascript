/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default  {
    id:1,
    firstName: 'firstName',
    lastName: 'lastName',
    facilityContactId:"123456781234567899",
    facilityId:'123456781234567899',
    facilityName:"facilityName",
    partnerAccountId:"123456781234567899",
    sellDate:'11/20/2015',
    installDate:'11/24/2015',
    invoiceNumber:"11",
    customerBrandName:"name",
    customerSourceId:22,
    managementCompanyId:"21",
    buyingGroupId:"25",
    partnerRepUserId:'uid',
    invoiceUrl:"url",
    isSubmitted:false,
    url: 'https://dummy-url.com',

    simpleLineItemId:2,
    assetId:"123",
    serialNumber:"344",
    productLineId:340,
    price:10,
    productLineName:"line1",
    productGroupId:300,
    productGroupName:"group1"


};




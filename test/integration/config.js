import PartnerSaleRegistrationDraftServiceSdkConfig from '../../src/partnerSaleRegistrationDraftServiceSdkConfig';

export default {
    partnerSaleRegistrationDraftServiceSdkConfig: new PartnerSaleRegistrationDraftServiceSdkConfig(
        'https://api-dev.precorconnect.com'
    ),
    identityServiceJwtSigningKey: 'nbho9k9vcv8r48xGQs4woyN8BJ6q9X1efj295KXfS9A9yHJSRm0oU21j3ickrScQ',
    accountIdOfExistingAccountWithAnSapAccountNumber: '001K000001H2Km2IAF',
    existingCustomerSegmentId: 1

}

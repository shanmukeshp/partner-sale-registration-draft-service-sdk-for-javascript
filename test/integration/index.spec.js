import PartnerSaleRegistrationDraftServiceSdk,{
    AddPartnerCommercialSaleRegDraftReq,
    PartnerCommercialSaleRegSynopsisView,
    PartnerCommercialSaleRegDraftView,
    UpdatePartnerCommercialSaleRegDraftReq,
    UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq,
} from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be PartnerSaleRegistrationDraftServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new PartnerSaleRegistrationDraftServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(PartnerSaleRegistrationDraftServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('addPartnerCommercialSaleRegDraft method', () => {
            it('should return ids', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleRegistrationDraftServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig);

                let addPartnerCommercialSaleRegDraftReq =
                    new AddPartnerCommercialSaleRegDraftReq(
                        dummy.facilityContactId,
                        dummy.facilityId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.invoiceNumber,
                        dummy.customerBrandName,
                        dummy.customerSourceId,
                        dummy.managementCompanyId,
                        dummy.buyingGroupId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .addPartnerCommercialSaleRegDraft(
                            addPartnerCommercialSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )
                        .then(ids => {
                            return ids;
                        });

                /*
                 assert
                 */
                actPromise
                    .then((ids) => {
                        expect(ids).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000);
        });

        describe('getPartnerSaleRegistrationDraftWithDraftId method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleRegistrationDraftServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig);

                let addPartnerCommercialSaleRegDraftReq =
                    new AddPartnerCommercialSaleRegDraftReq(
                        dummy.facilityContactId,
                        dummy.facilityId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.invoiceNumber,
                        dummy.customerBrandName,
                        dummy.customerSourceId,
                        dummy.managementCompanyId,
                        dummy.buyingGroupId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addPartnerCommercialSaleRegDraft(
                            addPartnerCommercialSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )
                        .then(ids => {
                            return ids;
                        });

                /*
                 act
                 */
                const partnerSaleRegistrationDraftWithDraftIdPromise =
                    actPromise
                        .then(ids =>{
                                return objectUnderTest
                                    .getPartnerSaleRegistrationDraftWithDraftId(
                                        ids,
                                        factory
                                            .constructValidPartnerRepOAuth2AccessToken()
                                    );
                            }
                        );


                /*
                 assert
                 */
                partnerSaleRegistrationDraftWithDraftIdPromise
                    .then((PartnerCommercialSaleRegSynopsisView) => {
                        expect(PartnerCommercialSaleRegSynopsisView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });
        describe('submitPartnerCommercialSaleRegDraft method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleRegistrationDraftServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig);

                let addPartnerCommercialSaleRegDraftReq =
                    new AddPartnerCommercialSaleRegDraftReq(
                        dummy.facilityContactId,
                        dummy.facilityId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.invoiceNumber,
                        dummy.customerBrandName,
                        dummy.customerSourceId,
                        dummy.managementCompanyId,
                        dummy.buyingGroupId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addPartnerCommercialSaleRegDraft(
                            addPartnerCommercialSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )
                        .then(ids => {
                            return ids;
                        });

                /*
                 act
                 */
                const partnerCommercialSaleRegDraftWithAccountIdPromise =
                    actPromise.then( ids =>{
                            return objectUnderTest
                                .submitPartnerCommercialSaleRegDraft(
                                    ids,
                                    factory.constructValidPartnerRepOAuth2AccessToken()
                                );
                        }
                    );


                /*
                 assert
                 */
                partnerCommercialSaleRegDraftWithAccountIdPromise
                    .then((PartnerCommercialSaleRegDraftView) => {
                        expect(PartnerCommercialSaleRegDraftView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });


        describe('getPartnerCommercialSaleRegDraftWithAccountId method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleRegistrationDraftServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig);


                /*
                 act
                 */
                const partnerCommercialSaleRegDraftWithAccountIdPromise =
                    objectUnderTest
                        .getPartnerCommercialSaleRegDraftWithAccountId(
                            //'123456781234567899',
                            config.accountIdOfExistingAccountWithAnSapAccountNumber,
                            factory
                                .constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */
                partnerCommercialSaleRegDraftWithAccountIdPromise
                    .then((PartnerCommercialSaleRegSynopsisView) => {
                        expect(PartnerCommercialSaleRegSynopsisView.length >= 1).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        describe('updatePartnerCommercialSaleRegDraft method', () => {
            it('should return PartnerCommercialSaleRegSynopsisView', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleRegistrationDraftServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig);

                let addPartnerCommercialSaleRegDraftReq =
                    new AddPartnerCommercialSaleRegDraftReq(
                        dummy.facilityContactId,
                        dummy.facilityId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.invoiceNumber,
                        dummy.customerBrandName,
                        dummy.customerSourceId,
                        dummy.managementCompanyId,
                        dummy.buyingGroupId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addPartnerCommercialSaleRegDraft(
                            addPartnerCommercialSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )
                        .then(ids => {
                            return ids;
                        });

                const updatePartnerCommercialSaleRegDraftReq =
                    new UpdatePartnerCommercialSaleRegDraftReq(
                        dummy.id,
                        dummy.facilityContactId,
                        dummy.facilityId,
                        dummy.facilityName,
                        dummy.customerBrandName,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.invoiceNumber,
                        dummy.customerSourceId,
                        dummy.managementCompanyId,
                        dummy.buyingGroupId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                /*
                 act
                 */
                const partnerCommercialSaleRegDraftWithAccountIdPromise =
                    actPromise.then( ids =>{
                            return objectUnderTest
                                .updatePartnerCommercialSaleRegDraft(
                                    updatePartnerCommercialSaleRegDraftReq,
                                    ids,
                                    factory
                                        .constructValidPartnerRepOAuth2AccessToken()
                                );
                        }
                    );


                /*
                 assert
                 */
                partnerCommercialSaleRegDraftWithAccountIdPromise
                    .then((PartnerCommercialSaleRegSynopsisView) => {
                        expect(PartnerCommercialSaleRegSynopsisView).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        describe('deletePartnerSaleRegDraftWithId method', () => {
            it('should return PartnerCommercialSaleRegSynopsisView', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleRegistrationDraftServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig);

                let addPartnerCommercialSaleRegDraftReq =
                    new AddPartnerCommercialSaleRegDraftReq(
                        dummy.facilityContactId,
                        dummy.facilityId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.invoiceNumber,
                        dummy.customerBrandName,
                        dummy.customerSourceId,
                        dummy.managementCompanyId,
                        dummy.buyingGroupId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addPartnerCommercialSaleRegDraft(
                            addPartnerCommercialSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )
                        .then(ids => {
                            return ids;
                        });

                /*
                 act
                 */
                const partnerCommercialSaleRegDraftWithAccountIdPromise =
                    actPromise.then( ids=> {
                            return objectUnderTest
                                .deletePartnerSaleRegDraftWithId(
                                    ids,
                                    factory
                                        .constructValidPartnerRepOAuth2AccessToken()
                                )
                        }
                    );

                /*
                 assert
                 */
                partnerCommercialSaleRegDraftWithAccountIdPromise
                    .then((string) => {
                        expect(string).toEqual('Draft deleted successfully');
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        describe('updateSaleInvoiceUrlOfPartnerSaleRegDraft method', () => {
            it('should return success message', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleRegistrationDraftServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig);

                let addPartnerCommercialSaleRegDraftReq =
                    new AddPartnerCommercialSaleRegDraftReq(
                        dummy.facilityContactId,
                        dummy.facilityId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.invoiceNumber,
                        dummy.customerBrandName,
                        dummy.customerSourceId,
                        dummy.managementCompanyId,
                        dummy.buyingGroupId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addPartnerCommercialSaleRegDraft(
                            addPartnerCommercialSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )
                        .then(ids => {
                            return ids;
                        });

                /*
                 act
                 */
                const addPartnerCommercialSaleRegDraftPromise =
                    actPromise.then( ids=> {
                            return objectUnderTest
                                .updateSaleInvoiceUrlOfPartnerSaleRegDraft(
                                    new UpdateSaleInvoiceUrlOfPartnerSaleRegDraftReq(
                                        ids,
                                        "http://www.new-dummy-url.com"
                                    ),
                                    factory
                                        .constructValidPartnerRepOAuth2AccessToken()
                                )
                        }
                    );

                /*
                 assert
                 */
                addPartnerCommercialSaleRegDraftPromise
                    .then((string) => {
                        expect(string).toEqual('Invoice Url updated successfully');
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });

        describe('removeSaleLineItemFromPartnerSaleRegDraft method', () => {
            it('should return success message', (done) => {

                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerSaleRegistrationDraftServiceSdk(config.partnerSaleRegistrationDraftServiceSdkConfig);


                let addPartnerCommercialSaleRegDraftReq =
                    new AddPartnerCommercialSaleRegDraftReq(
                        dummy.facilityContactId,
                        dummy.facilityId,
                        dummy.facilityName,
                        dummy.partnerAccountId,
                        dummy.sellDate,
                        dummy.installDate,
                        dummy.invoiceNumber,
                        dummy.customerBrandName,
                        dummy.customerSourceId,
                        dummy.managementCompanyId,
                        dummy.buyingGroupId,
                        dummy.partnerRepUserId,
                        dummy.invoiceUrl,
                        dummy.isSubmitted,
                        factory.constructSimpleLineItems(),
                        factory.constructCompositeLineItems()
                    );

                // seed a new account so we can test the retrieval of it
                const actPromise =
                    objectUnderTest
                        .addPartnerCommercialSaleRegDraft(
                            addPartnerCommercialSaleRegDraftReq,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )
                        .then(ids => {
                            return ids;
                        });

                /*
                 act
                 */

                    actPromise
                        .then(ids => {
                                return objectUnderTest
                                    .getPartnerSaleRegistrationDraftWithDraftId(
                                        ids,
                                        factory.constructValidPartnerRepOAuth2AccessToken()
                                    ).then(PartnerCommercialSaleRegSynopsisView=> {
                                            PartnerCommercialSaleRegSynopsisView.compositeLineItems[0].id;
                                        }
                                    );
                            }
                        );

                /*
                 assert
                 */
                const partnerSaleRegDraftSaleLineItemIdPromise =
                    actPromise
                        .then(partnerSaleRegDraftSaleLineItemId =>{
                            return objectUnderTest
                                        .removeSaleLineItemFromPartnerSaleRegDraft(
                                            partnerSaleRegDraftSaleLineItemId,
                                            factory.constructValidPartnerRepOAuth2AccessToken()
                                        )
                            }
                        );


                partnerSaleRegDraftSaleLineItemIdPromise
                    .then((string) => {
                        expect(string).toEqual('partner sale line item removed successfully');
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 20000)
        });
    });
});